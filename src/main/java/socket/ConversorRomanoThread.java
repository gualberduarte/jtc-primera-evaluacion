package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import datos.LogConversiones;
import datos.LogConversionesManager;
import romano.ConversorRomano;
import utils.LogUtil;

public class ConversorRomanoThread extends Thread {
    
	//Referencia al cliente que sera atendido
    private Socket cliente;
    
    public ConversorRomanoThread(Socket cliente) {
        this.cliente = cliente;
    }
    
    @Override
    public void run() {
    	LogUtil.INFO("Nueva peticion recibida...");
  
    	try{

    					OutputStream clientOut = cliente.getOutputStream();
    					PrintWriter pw = new PrintWriter(clientOut, true);

    					InputStream clientIn = cliente.getInputStream();
    					BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
    					String mensajeRecibido = br.readLine();
    					

    					String nomThread;
    					String IP;
    					int numeroConv;
    					String responseMsg;
    					LogConversiones bean;

 
    					if (mensajeRecibido != null) {
    						if (mensajeRecibido.trim().equalsIgnoreCase("fin")) {
    							pw.println("Fin del programa!.");
    							System.exit(0);
    							
    						} else if (mensajeRecibido.trim().startsWith("conv")) {
    							String strnum = mensajeRecibido.substring(4);
    							numeroConv = Integer.parseInt(strnum.trim());
    							try {
    								responseMsg = "El valor convertido es: " + ConversorRomano.decimal_a_romano(numeroConv);
    								pw.println(responseMsg);
    							} catch (Exception e) {
    								responseMsg = "Hubo un error: " + e.getMessage();
    							}
    							IP = cliente.getInetAddress().getHostAddress();
    							nomThread = this.nombreThread();
    							bean = new LogConversiones(nomThread, IP, numeroConv, responseMsg);

    							LogConversionesManager lcm = new LogConversionesManager();
    							lcm.insertarNuevoRegistro(bean);
    							pw.println("Procesado");
    						}
    						pw.println("Mensaje recibido no valido, reintente por favor!");
    					} 
    					

    					clientIn.close();
    					clientOut.close();
    					cliente.close();
    				} catch (IOException ie) {
    					System.out.println("Error al procesar Thread. ");
    		        }
    	
    	LogUtil.INFO("Finalizando atencion de la peticion recibida...");
    }
    public String nombreThread(){
    	LogConversionesManager contador = new LogConversionesManager();
    	return "Converción " + contador.getCantidadRegistros();
    }
}
