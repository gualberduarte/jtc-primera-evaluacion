package socket;

import java.net.*;
import java.io.*;
import romano.ConversorRomano;
import utils.LogUtil;

/**
 * Clase ConversorRomanoServer Multihilo
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ConversorRomanoMultihilo {

    public static void main(String[] args) {
        ServerSocket servidor = null;
        Socket cliente;
        
        try {
        	LogUtil.INFO("Iniciando socket server en puerto 6400");
            servidor = new ServerSocket(6400);
        } catch (IOException ie) {
            System.err.println("Error al abrir socket. " + ie.getMessage());
            System.exit(1);
        }
        
        LogUtil.INFO("Socket iniciado, se atienden peticiones..");
        ConversorRomanoThread theread;
               while (true) {        
        	        try {
        				cliente = servidor.accept();
        				
        				theread = new ConversorRomanoThread(cliente);
        				theread.start();
        				
        			} catch (IOException e) {
        				System.out.println("Error al procesar socket.");
        			}
                }
    }

}