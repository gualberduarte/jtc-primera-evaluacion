package utils;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Clase JdbcUtil
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class JdbcUtil {
    
    private static JdbcUtil instancia = null;
    private String cadenaConexion;
    private String usuario;
    private String password;
    
    public JdbcUtil() {
        try {
            cadenaConexion = "jdbc:derby://localhost:1527/BDBITACORA";
            usuario = "romanoUser";
            password = "romanoPass";
            Class.forName("org.apache.derby.jdbc.ClientDriver");
        } catch (Exception e) {
            LogUtil.ERROR("Error al construir instancia de JdbcUtil", e);
            System.err.println("Error al construir JdbcUtil");
            System.exit(1);
        }
    }
    
    /**
     * Retorna un nuevo objeto connection a la base de datos.
     * Al terminar de usar la conexion, debe darle close().
     * @return Conexion a la base de datos o NULL en caso de error
     */
    public Connection getConnection() {
        Connection conn = null;
        
        try {
            conn = DriverManager.getConnection(cadenaConexion, usuario, password);
        } catch (Exception e) {
            LogUtil.ERROR("Error al obtener conexion a base de datos", e);
            System.out.printf("\nError al obtener conexion a base de datos.");
        }
        
        return conn;
    }
    
    /**
     * Metodo que retorna una instancia a JdbcUtil
     * @return Instancia a JDBC Util
     */
    public static JdbcUtil getInstance() {
        if (instancia == null) {
            instancia = new JdbcUtil();
            return instancia;
        } else {
            return instancia;
        }
    }
    
} //Fin de clase JdbcUtil
